---
layout: post
title: "Linux builds are online on openSUSE Build Service"
date: 2020-12-02 16:42:00
---

<p>
I am happy to announce that linux builds are online <a href="https://build.opensuse.org/project/show/home:maxrd2">openSUSE Build Service</a>.
</p>
Currently there are scripts building binaries for:
<ul>
	<li>Arch Linux (x86_64)</li>
	<li>Debian 10, Unstable and Testing</li>
	<li>openSUSE Tumbleweed (i586/x86_64) and Leap 15.1/15.2 (x86_64, PowerPC)</li>
	<li>Ubuntu Bionic (18.04 amd64), Focal (20.04 amd64) and Groovy (20.10 amd64)</li>
</ul>
<p>
Linux AppImage and mingw64 Windows builds are planned next.
They are of course all available on <a href="/download.html">download</a> page.
</p>
<p class="author">— Mladen</p>
