---
layout: post
title: "Subtitle Composer 0.8.0 released"
date: 2023-11-06 19:21:40

---

I'm happy to announce the 0.8.0 release of Subtitle Composer.

This release contains lots of bugfixes and new features including:

- Automatic translations using DeepL or Google Cloud
- WebVTT format support
- Subtitle positioning UI and support
- Subtitle class/comments/voice UI and support
- Improved rich text editing/preview
- Rich text rendering on waveform/video/editors
- Qt6 support
- FFmpeg 5 support
- Subtitle lines are always time sorted; remove sort lines menu action
- Replaced Kross with QJSEngine, removed ruby and python support
- Improved script manager and tools menu

As usual all binaries are available from [download page](/download.html).

Source tarball can be downloaded from [download.kde.org](https://download.kde.org/stable/subtitlecomposer/).

<p class="author">— Mladen</p>
