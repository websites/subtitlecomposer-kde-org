---
layout: post
title: "Subtitle Composer 0.7.1 released"
date: 2021-10-05 09:21:40

---

I'm happy to announce the 0.7.1 release of Subtitle Composer.

This release contains lots of bugfixes and new features including:

- Improved stability, look and feel on different desktop environments
- Fixed various bugs and rare crashes
- Fixed Undo stack and improved text editing undo
- Improved/replaced video player (performance, Wayland support, OpenGL/FFmpeg)
- Improved waveform performance
- Improved subtitle lines widget and model performance
- Improved Wayland support
- Open/Save actions are using native file dialogs
- Improved text charsets/encodings/end-of-line selection, detection and handling
- Improved VobSub support
- Improved inline editor supports text styles
- Improved subtitle style rendering
- Improved character rate display and added coloring
- Improved command line - ability to open all subtitle/media files
- Added pause/duration columns to list view
- Updated/added many translations

As usual all binaries are available from [download page](/download.html).

Source tarball can be downloaded from [download.kde.org](https://download.kde.org/stable/subtitlecomposer/).

<p class="author">— Mladen</p>
