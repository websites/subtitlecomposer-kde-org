---
layout: post
title: "WebVTT support, CSS and more"
date: 2022-11-12 20:33:00

---

I'm happy to announce that the support for WebVTT format and all the nice features that it brings is finished.

Along with reading and writing of WebVTT subtitles, Subtitle Composer can now fully understand CSS - Cascading Style Sheets, subtitle positions and alignment.

A new panel has been added that allows syntax-higlighted CSS editing and subtitle position/alignment adjustment. Of course all edits are visible everywhere in app in realtime.

Some user interface elements are still being worked on and should be available in following days, in particular:
 - Buttons for assigning class/voice tags to selected text
 - Intuitive video overlay with handles to easily drag/size individual subtitle areas
 - Ability to show/hide style/class/voice tags/elements

Release 0.8.0 is planned once above list is done and potential bugs that come up are resolved.
Depending on community interest - ASS/SSA format improvements could be done to benefit from things that WebVTT support brought.

As usual you can try out precompiled binaries or AppImage of git development version (the `subtitlecomposer-git` package)
from [download page](/download.html) or build it yourself from  [invent.kde.org](https://invent.kde.org/multimedia/subtitlecomposer).

<p class="author">— Mladen</p>
