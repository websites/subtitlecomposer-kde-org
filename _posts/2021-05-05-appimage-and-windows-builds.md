---
layout: post
title: "AppImage and Windows builds are online"
date: 2021-05-05 00:00:00
---

After Windows/AppImage builds got broken on Travis CI due to Ubuntu Trusty EOL - new build scripts have been made.

Linux AppImage is now being built on [openSUSE Build Service](https://build.opensuse.org/project/show/home:maxrd2).

Windows build is in process of being added to OBS - in meantime builds are being done manually in [docker container](https://github.com/maxrd2/arch-mingw) by yours truly.
If anyone's interested the relative [script is here](https://invent.kde.org/multimedia/subtitlecomposer/-/blob/master/pkg/mingw/build.sh).

Also some fixes have been made to Subtitle Composer UI and themes - it looks good now in all Linux desktop environments, AppImage and Windows.

As usual all binaries are available from [download page](/download.html).

<p class="author">— Mladen</p>
