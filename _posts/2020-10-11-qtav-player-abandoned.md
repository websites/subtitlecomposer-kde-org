---
layout: post
title: "New QtAV player has been abandoned"
date: 2020-10-11 18:29:57
---

As you might have noticed - all various video player plugins have recently been removed from Subtitle Composer.

They were all using external video players or libraries and each one of them had it share of problems integrating
and working nicely with Subtitle Composer. Different handling of audio volume, frequency of notifications, seek
precision, error handling/reporting and so on. Also none was working under Wayland.

Also using different backends made it very time consuming to test changes in Subtitle Composer.

After moving the project to KDE build infrastructure I had to make build scripts of various video backends and
their dependencies. That would likely amount to 100+ new packages/build scripts and who knows what else.

Instead - have decided to drop all those backend and use QtAV - library made for Qt that would handle all FFmpeg
calls and rendering. QtAV seemed great at the moment, but it ended up much worse than expected.

 - It has one big mess of qmake/cmake/shell/batch build scripts that was close to impossible to build using qmake inside
   [KDE Craft](https://community.kde.org/Craft) on all supported platforms. After few weeks of debugging, testing and
   [patching](https://invent.kde.org/packaging/craft-blueprints-kde/-/commits/967b59ee19bfa91b432cb9d7c7a99181ae09d276/libs/qtav/qtav.py)
   qmake scripts I've deleted it all and made it using cmake build with far
   [less patches](https://invent.kde.org/packaging/craft-blueprints-kde/-/commit/967b59ee19bfa91b432cb9d7c7a99181ae09d276).

 - After Subtitle Composer relases started going out people started complaining about performance, stuttering, compilation
   and so on.

 - APIs ended up being worse from what is seemed at first and seeking to correct position in file was awful. Accurate seeking
   is imporant for a subtitle editor.

After inspecting QtAV code when I was trying to patch/fix things I've decided to - remove it all again. Now Subtitle Composer
uses FFmpeg libraries directly for decoding media streams and does all rendering and color conversion using OpenGL shaders.
You can read all about adventures with color conversion in a post I'll write soon.

Some people preferred using GStreamer. Over the years I had experiences with GStreamer and it's plugins not being able to open
all formats/containers/codecs (probably due to broken/bad files where FFmpeg was much more tolerant and robust) and sometimes
even crashing. Also GStreamer is designed in similar fashion to Microsoft's DirectShow - it has a pipeline with bunch of
decoders/encoders/sources/sink which is very nice for hacking stuff from command line, but find it disgusting to code with.

<p class="author">— Mladen</p>
