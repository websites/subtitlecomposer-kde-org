---
layout: page
title: Features
sorted: 5
---

# **What can {{ site.title }} do?**
---
<br>

  - Open/Save **Text Subtitle Formats**
    - SubRip/SRT, WebVTT, MicroDVD, SSA/ASS, MPlayer, TMPlayer and YouTube captions
  - **OCR/Open Graphics Subtitle Formats**
    - VobSub (.idx/.sub/.rar), BluRay/PGS (*.sup), formats supported by ffmpeg (DVD/Vob, DVB, XSUB, HDMV-PGS)
  - **Demux Graphics/Text Subtitle Stream** from video file
    - SRT, SSA/ASS, MOV text, MicroDVD, Graphic formats supported by ffmpeg (DVD/Vob, DVB, XSUB, HDMV-PGS)
  - **Speech Recognition** from audio/video file using PocketSphinx
  - Smart **Language/Text encoding** detection
  - **Integrated Video Player** with live subtitle preview, lots of supported formats (FFmpeg), audio stream selection
  - Preview/editing of subtitles on **Audio Waveform** with audio stream selection
  - Quick and **easy subtitle sync**:
    - Dragging several anchors/graftpoints and stretching timeline
    - Time shifting and scaling, lines duration re-calculation, framerate conversion, etc.
    - Joining and splitting of subtitle files
  - **Side-by-side** subtitle translations/editing
  - Text **styles** (italic, bold, underline, stroke, color), **CSS classes** and **WebVTT voice tags**
  - Subtitle position/alignment editing and live preview
  - **Spell checking**
  - Detection of timing errors in subtitles
  - **JavaScript** scripting support
