---
layout: page
title: Get Involved
konqi: /assets/img/konqi-dev.png
sorted: 5
---

# **Want to contribute to {{ site.title }}?**
---

#### Get in touch
There's now a **Matrix chat group** for {{ site.title }}, namely [#subtitlecomposer](https://webchat.kde.org/#/room/#subtitlecomposer:kde.org). Everyone is welcome, so jump in and say hello!

**Feedback or ideas** on how to make {{ site.title }} better are welcome and appreciated! Let us know in [#subtitlecomposer](https://webchat.kde.org/#/room/#subtitlecomposer:kde.org) or [issue tracker](https://invent.kde.org/multimedia/subtitlecomposer/issues)
<br>

#### Report Bugs / Request Features
**Bug reports or feature requests** can be submitted to the [issue tracker](https://invent.kde.org/multimedia/subtitlecomposer/issues).

#### Write some Code
Our main repository is hosted [under KDE Invent](https://invent.kde.org/multimedia/subtitlecomposer/). You will need a [KDE Identity account](http://identity.kde.org/) to submit changes and do pull requests.

Be a champ and please follow our [coding style](https://invent.kde.org/multimedia/subtitlecomposer/-/blob/master/README.CodingStyle.md), yeah? :)

#### Help Translating {{ site.title }}
**Translators** can learn how to get involved [here](https://community.kde.org/Get_Involved/translation).<br>
The main thing you always need to do is get in contact with the [localization team on IRC](irc://chat.freenode.net/kde-i18n), they'll help you get started.<br>
Note that you can contact people over IRC through [Matrix](https://webchat.kde.org/#/room/#freenode_#kde-i18n:matrix.org) too!

#### Help us with Documentation and Tutorials
**Video tutorials** are very welcome as are any kind of documentation/tutorials/examples, please let us know if you make some. We may even display it on this website!

#### Help with {{ site.title }} website
**Website** stuff can be discussed with the main developer and the main website maintainer who hang out on [our Matrix group](https://webchat.kde.org/#/room/#subtitlecomposer:kde.org), but you can also contact the #kde-web [Matrix group](https://webchat.kde.org/#/room/#freenode_#kde-www:matrix.org), [Telegram group](https://t.me/KDEWeb) or [IRC group](irc://chat.freenode.net/kde-www), all three are bridged. You can also send patches directly to the [main website repository](https://invent.kde.org/websites/subtitlecomposer-kde-org).
