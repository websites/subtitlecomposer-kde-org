# Subtitle Composer website

## Build instruction

Install the following packages depending on your distribution:
```
# Debian/Ubuntu:
sudo apt install ruby-dev bundler git

# openSUSE:
sudo zypper install ruby-devel bundler git

# Fedora:
sudo dnf install ruby-devel redhat-rpm-config git

# Arch:
sudo pacman -S ruby ruby-bundler git
```

Clone and install the required ruby dependencies:

```
git clone https://invent.kde.org/websites/subtitlecomposer-kde-org.git
cd subtitlecomposer-kde-org
gem install --user bundler jekyll
bundle config set path 'vendor/bundle'
bundle install
```

For further instructions or troubleshooting, refer to [the KDE Jekyll documentation](https://community.kde.org/KDE.org/Jekyll)

## Run development

```
bundle exec jekyll serve
```

By default it should run on http://127.0.0.1:4000/

## Run production

```
bundle exec jekyll build
```

The configurations are located in `_config.yml`. You should also change the path to the theme in the Gemfile.

---
Screenshots
---

![](assets/img/light1.png)
![](assets/img/dark1.png)
![](assets/img/light2.png)
![](assets/img/dark2.png)
![](assets/img/light3.png)
![](assets/img/dark3.png)
![](assets/img/light4.png)
![](assets/img/dark4.png)
